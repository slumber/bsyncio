[![pipeline status](https://gitlab.com/slumber/bsyncio/badges/master/pipeline.svg)](https://gitlab.com/slumber/bsyncio/commits/master)

# Blender asyncio wrapper operators
## Why ?

By default blender freeze when `loop.run_forever()` is called. While running endless tasks in blender can be really usefull, bsyncio aim to make is possible.

## Installation

- Get bsyncio.py from the repo
- import bsyncio:
    ~~~Python
        from . import bsyncio
    ~~~

- register bsyncio operators
    ~~~Python
        bsyncio.register()
    ~~~

## Uses

Now instead of calling `loop.run_forever()` in your blender addons, just call `bpy.ops.asyncio.loop()` to start asyncio loop and `bpy.ops.asyncio.stop()` to stop it.

## Example

~~~Python
    import bsyncio
    bsyncio.register()

    async def test_endless_task():
    while 1:
        print(time.time())
        await asyncio.sleep(1)

    loop = asyncio.get_event_loop()
    asyncio.ensure_future(test_server_task())

    if __name__ == "main":
        try:
            loop.run_forever()
        finally:
            loop.close()
~~~