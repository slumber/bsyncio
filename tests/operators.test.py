import unittest
import bpy
import os
import sys

sys.path.append(os.getcwd())
import bsyncio
import asyncio
import time

async def long_task(future):
    await asyncio.sleep(2)
    future.set_result('done')

class TestBsyncioOperators(unittest.TestCase):
    def test_loopEmpty(self):
        bpy.ops.asyncio.stop()
        bpy.ops.asyncio.loop()
        self.assertTrue(bsyncio._loop_kicking_operator_running)

    def test_loopStop(self):
        bpy.ops.asyncio.stop()
        self.assertFalse(bsyncio._loop_kicking_operator_running)

    def test_loopLongtask(self):
        successs = False
        future = asyncio.Future()
        asyncio.ensure_future(long_task(future))
        bpy.ops.asyncio.loop()
        # while(not future.done()):
        #     pass
        
        self.assertFalse(bsyncio._loop_kicking_operator_running)

if __name__ == '__main__':
    bsyncio.register()
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestBsyncioOperators)
    unittest.TextTestRunner(verbosity=2).run(suite)